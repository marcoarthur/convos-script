package Convos::Script;
use Mojo::Base -base, -strict, -signatures;
use RxPerl::IOAsync qw(:all);
use IO::Async::Loop;
use Mojo::File;
use Mojo::Exception qw(raise);
use Mojo::Log;
use Mojo::Pg;
use Data::Dumper;
use Carp;
use List::Util qw(any);
use YAML 'LoadFile';
use DateTime;
use Syntax::Keyword::Try;
use Convos::TailEvent;

our $VERSION = "0.01";

has conf            => sub { die "Need a conf file" };
has log             => sub { Mojo::Log->new };
has loop            => sub { IO::Async::Loop->new };
has options         => sub { die "Need setup options" };
has pg              => sub { die "Need a db connection" };
has observer        => sub ($self) {
    return {
        next => sub ($row) {
            $self->log->debug(
                sprintf(
                    "inserting new row\n%s", Dumper($row)
                )
            );
            if ( any { not defined($row->{$_}) } qw/nick message date/ ) {
                $self->log->error("Bad register. Skip sending to database") and return;
            }
            try { $self->pg->db->insert('convos.message', $row); } 
            catch($err) {
                $self->log->error("Error inserting data into database: $err");
            }
        },
        # TODO: what should throw error in the event stream ?
        error => sub($err) {
            raise __PACKAGE__ . '::Error',"Error observed: $err"
        },
        complete => sub { $self->loop->stop; },
    };
};

sub setup($self) {
    croak "Conf file don't exist or is not readable" unless -e $self->conf;
    $self->options(LoadFile($self->conf) or croak "Could not read conf file :$!");
    $self->log->level($self->options->{log_level} || 'warn');
}

sub new($class, @args) {
    my $self = $class->SUPER::new(@args);
    $self->setup;
    $self->pg(Mojo::Pg->new($self->options->{db_url} or croak "Can't connect to db"));
    $self->log->debug("Connected to database, finished script setup");
    return $self;
}

sub current_date($self) {
    my $date = DateTime->now;
    return ($date->year, sprintf("%02d", $date->month));
}

sub start ($self) {
    # definitions
    my $loop = $self->loop;
    my $conf = $self->options;
    my $db_observer = $self->observer;
    RxPerl::IOAsync::set_loop($loop);

    # file selection and observables setup
    my ($year, $month) = $self->current_date;
    my $convos_logs = Mojo::File->new($conf->{data_dir})
    ->list_tree({max_depth => 5, dir => 0})
    ->grep(
        sub {
            $_->extname eq 'log' && $_->basename =~ /^#/  && $_->to_array->[-3] eq $year && $_->to_array->[-2] eq $month
        }
    ) # filter chat files in current month/year
    ->map(
        sub {
            $self->log->debug("watching file $_");
            return Convos::TailEvent->new( file => $_, ioloop => $loop, log => $self->log );
        }
    ) # create TailEvent objects to watch file change
    ->map(
        sub{ 
            my $source = rx_from_event($_, 'on_arrival');
            $source->subscribe($db_observer); 
            $self->log->debug("attached observer to " . $_->file);
            return $source;
        }
    ); # assign $db_observer to react for on_arrival event

    $self->log->debug(
        sprintf (
            "starting script with configuration data\n%s",
            Dumper($conf)
        )
    );
    $loop->run;
    return $self;
}

sub stop ($self) {
    $self->log->debug("Stopping script");
    $self->loop->stop;
}

1;
__END__

=encoding utf-8

=head1 NAME

Convos::Script - Script to watch convos data files and save to database

=head1 SYNOPSIS

    use Convos::Script;

=head1 DESCRIPTION

Convos::Script is ...

=head1 LICENSE

Copyright (C) Marco Silva.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 AUTHOR

Marco Silva E<lt>arthurpbs@gmail.comE<gt>

=cut
