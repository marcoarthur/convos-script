package Convos::TailEvent;
use Mojo::Base 'Mojo::EventEmitter', -strict, -signatures;
use IO::Async::FileStream;
use DateTime::Format::Strptime;

has ioloop        => sub { die "needs io loop" };
has file          => sub { die "needs a file" };
has poll_interval => 0.5;
has log           => sub { die "needs logger" };

sub arrived_msg($self, $line) {
    $self->log->debug('msg arrived');
    my ($date, $nick, $msg) = @{$self->_parse_irc_msg($line)};

    # TODO: should we emit error for a bad line ?
    # $self->emit(
    #     error => Mojo::Exception->new( qq{Error parsing $line} )
    # ) and return unless $date || $nick || $msg;

    my $data = { 
        date => "$date", 
        nick => $nick, 
        message => $msg, 
        room => $self->room,
        irc => $self->irc,
    };

    $self->emit( on_arrival => $data);
}

sub room($self) {
    return $self->file->basename;
}

sub irc($self){
    return $self->file->to_array->[-4];
}

sub setup($self) {
    my $stream = IO::Async::FileStream->new
    (
        filename => $self->file->to_string,
        interval => $self->poll_interval,

        on_initial => sub {
            my ( $stream ) = @_;
            $stream->seek_to_last( "\n" );
        },

        on_read => sub {
            my ( undef, $buffref ) = @_;

            while( $$buffref =~ s/^(.*\n)// ) {
                $self->arrived_msg($1);
            }
            return 0;
        },
    );

    $self->ioloop->add($stream);
}

sub new($class, @args) {
    my $self = $class->SUPER::new(@args);
    $self->setup;
    return $self;
}

sub _parse_irc_msg($self, $msg) {
    my $parts = [ $msg =~ m/^(.{19}) 0 <([^>]+)>(.+)$/ ];
    my $dt_parser = DateTime::Format::Strptime->new(
        pattern => '%Y-%m-%dT%H:%M:%S',
        on_error => 'croak',
    );

    if ( $parts->[0] ) {
        $parts->[0] = $dt_parser->parse_datetime($parts->[0]);
    }
    return $parts;
}

1;
