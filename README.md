
# NAME

Convos::Script - It's new $module

# SYNOPSIS

    use Convos::Script;

# DESCRIPTION

Convos::Script is ...

# LICENSE

Copyright (C) Marco Silva.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# AUTHOR

Marco Silva <arthurpbs@gmail.com>
