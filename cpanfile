requires 'Daemon::Control';
requires 'DateTime';
requires 'DateTime::Format::Strptime';
requires 'IO::Async::FileStream';
requires 'IO::Async::Loop';
requires 'List::Util';
requires 'Mojo::Base';
requires 'Mojo::Exception';
requires 'Mojo::File';
requires 'Mojo::Log';
requires 'Mojo::Pg';
requires 'RxPerl::IOAsync';
requires 'Syntax::Keyword::Try';
requires 'YAML';

on configure => sub {
    requires 'Module::Build::Tiny', '0.035';
    requires 'perl', '5.008_001';
};

on test => sub {
    requires 'DDP';
    requires 'File::Path';
    requires 'IPC::Run';
    requires 'Mojo::Collection';
    requires 'Mojo::Template';
    requires 'Test2::Tools::Spec';
    requires 'Test2::V0';
    requires 'Test::More', '0.98';
};
