#!/usr/bin/env perl

use Mojo::Base -strict, -signatures;
use lib qw(./lib);
use Convos::Script;
our $VERSION = "0.01";

my $c = Convos::Script->new(conf => $ARGV[0] || $ENV{CONVOS_SCRIPT_CONF});
$c->start;
