#!/usr/bin/env perl

use Mojo::Base -strict, -signatures;
use Daemon::Control;
use Mojo::File qw(curfile);
our $VERSION = "0.01";

my $path = curfile;
my $conf = $ENV{CONVOS_SCRIPT_CONF};
die "No configuration" unless -e $conf;

exit Daemon::Control->new(
    name        => "convos_db",
    lsb_start   => '$syslog $remote_fs',
    lsb_stop    => '$syslog',
    lsb_sdesc   => 'convos db message',
    lsb_desc    => 'controls convos daemon sending messages',
    path        => $path->dirname,

    program     => $path->dirname . '/irc2pgdb.pl',
    program_args => [$conf],

    pid_file    => '/tmp/mydaemon.pid',
    stderr_file => '/tmp/mydaemon.err',
    stdout_file => '/tmp/mydaemon.out',

    fork        => 2,

)->run;
