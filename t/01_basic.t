use Mojo::Base -strict;
use Test2::V0;
use Test2::Tools::Spec;
use Mojo::File;
use Mojo::Collection qw(c);
use File::Path qw(remove_tree);
use Convos::Script;
use Mojo::Template;
use DateTime;
use IPC::Run qw(start pump finish timeout run);
use DDP;

my $loop = IO::Async::Loop->new;
our $FILES; # Data files to be observed
our $FLIST_SIZE = 10;
our $CONF = Mojo::File->new('./t/data/config.yaml');
our $script = Convos::Script->new( conf => "$CONF", loop => $loop );
our $FLIST_TMPL = './t/data/files';
our $MNT = Mojo::File->new($script->options->{data_dir});

describe 'startup the daemon and listening the file changes' => sub {

    before_all once => sub {
        # create the files' layout set as t/data/files
        my $date = DateTime->now;
        my $tmpl = Mojo::Template->new(vars => 1);
        my $args = { month => sprintf ("%02d", $date->month), year => $date->year };

        my $all = c( 
            map { Mojo::File->new($_) } 
            split /\n/, 
            $tmpl->render(Mojo::File->new($FLIST_TMPL)->slurp('UTF-8'), $args)
        );
        # select 10 files randomly
        $FILES = c( map { $MNT->child("$_") } @{$all->shuffle}[1..$FLIST_SIZE] );
        $FILES->each( 
            sub { 
                $_->dirname->make_path unless -d $_->dirname;
                $_->touch unless -e $_;
            } 
        );
    };

    # remove the files created during test
    after_all once => sub { remove_tree( "$MNT" ) if -d $MNT; };

    # check configuration
    tests config_file_and_mnt_layout => sub {
        # test configuration is correct
        is $script->options, 
        { 
            db_url => 'postgresql://?service=sqitch',
            data_dir => './t/data/mnt',
            log_level => 'debug',
        },
        "Rigth configuration";

        # file layout is ok
        $FILES->each(sub { ok -f $_, "data file exists"; });
    };

    # start the daemon
    my $handle;
    tests start_daemon => sub {
        my @cmd = ( qw|perl ./script/daemon.pl|, "start" );
        $ENV{CONVOS_SCRIPT_CONF} = $CONF;
        my ($err, $in, $out);

        # $handle = start \@cmd, \undef, \$out, \$err;
        # ok $handle, "started daemon";
        my $ret = run \@cmd, \undef, \$out, \$err;
        ok 1, "started daemon.pl with '$ret' code";
        my $msg = '2024-03-01T10:08:49 0 <nick> hello world';
        my $f = $FILES->[1];
        my $fh = $f->touch->open('>');
        $fh->autoflush(1);
        ok $fh, "handle for $f file";
        ok $ret = $fh->print("$msg\n"), "wrote to $f file";
        sleep 3;
    };

    # make changes
    # tests changes_to_files => sub {
    # };

    # stop the daemon
    tests stop_daemon => sub {
        # $handle->finish;
        # ok $handle->finish, "stopped daemon";
        my @cmd = ( qw|perl ./script/daemon.pl|, "stop" );
        my ($err, $in, $out);
        sleep 2;
        my $ret = run \@cmd, \undef, \$out, \$err;
        ok 1, "stoped daemon.pl with '$ret' code";
    };

    #tests start_script_as_daemon => sub {
    #    my @cmd = (qw( perl ./script/daemon.pl ), "start");
    #    $ENV{CONVOS_SCRIPT_CONF} = $CONF;
    #    my ($err, $in, $out);
    #    my $h = start \@cmd, \undef, \$out, \$err;
    #    my $msg = '2024-03-01T10:08:49 0 <nick> hello world';
    #    my $f = $FILES->[0];
    #    my $fh = $FILES->[0]->touch->open('>');
    #    ok $h, "running script";
    #    sleep 3;
    #    #my $ret = print $fh $msg;
    #    $fh->autoflush(1);
    #    my $ret = $fh->print("$msg\n");
    #    ok $ret , "sending message '$msg' to $f";
    #    sleep 3;
    #    # note "$err";
    #    # note "$out";
    #    finish $h and ok 1, "finished script";
    #};
};

done_testing;
